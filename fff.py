import time

def my_task():
    start_time = time.time()
    for i in range(10):
        time.sleep(1)
    end_time = time.time()
    print(end_time - start_time)

my_task()
