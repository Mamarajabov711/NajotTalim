from threading import Thread
import time
COUNT = 5000
def my_task():
    start = time.time()
    def countdown(n):
        while n >0:
            n -=1
            
    for i in range(10):
        t1 = Thread(target=countdown, args=(COUNT//2,))
        t2 = Thread(target=countdown, args=(COUNT//2,))

        t1.start()
        t2.start()

        t1.join()
        t2.join()
        time.sleep(1)
    end = time.time()
    print(end - start)
my_task()
